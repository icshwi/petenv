import os
from typing import Tuple

from PyQt5 import QtGui, QtWidgets


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setWindowTitle("petenv")
        self.resize(1024, 768)
        self.setMinimumSize(800, 450)

        app_dir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QtGui.QIcon(app_dir + os.path.sep + "icon.png"))

        self.layout = QtWidgets.QHBoxLayout()
        widget = QtWidgets.QWidget(self)
        widget.setLayout(self.layout)
        self.setCentralWidget(widget)

    def add_widget(self, widget):
        self.layout.addWidget(widget)


class Alert(QtWidgets.QMessageBox):
    def __init__(
        self, title: str = None, text: Tuple[str, str] = None, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.setWindowTitle(title)
        self.setIcon(QtWidgets.QMessageBox.Critical)

        self.setFont(QtGui.QFont("Monospace", 10))
        self.setText(text[0])
        self.setInformativeText(text[1])

        self.exec()
