import logging
import os
from io import IOBase
from typing import Any, NoReturn

import epics
from epics import caget, caput
from opcua import Node
from PyQt5 import QtWidgets

from petenv.gui.widget import TreeWidget, Widget
from petenv.gui.window import Alert

logger = logging.getLogger()


def my_printf_handler(output_str: str):
    logger.debug(output_str)


epics.ca.replace_printf_handler(my_printf_handler)


class IOCTreeWidget(TreeWidget):
    def __init__(self, pv_list: str, parent=None):
        super().__init__(name="IOC", parent=parent)
        self.pv_list = pv_list
        self.setup()

    def setup(self) -> NoReturn:
        super().setup()
        self.build_tree(None, None)

    def build_tree(self, branch, parent) -> NoReturn:  # todo: refactor to use params
        devices = {}
        top_level_items = []

        try:
            with open(self.pv_list, "r") as file:
                lines = file.readlines()
        except FileNotFoundError:
            raise FileNotFoundError(f"The file {self.pv_list} does not seem to exist.")
        for line in lines:
            if not line.strip():
                continue
            split_line = line.strip().split(":")
            if len(split_line) > 3:
                raise IndexError

            root = QtWidgets.QTreeWidgetItem([split_line[0]])  # todo: why list?

            if root.text(0) not in top_level_items:
                self.addTopLevelItem(root)
                top_level_items.append(root.text(0))

            root = self.topLevelItem(top_level_items.index(root.text(0)))

            # Check if this is autosave stuff
            if split_line[0][-3:] == "-as":
                field = split_line[1]
                leaf = QtWidgets.QTreeWidgetItem([field])
                root.addChild(leaf)
                continue

            if len(split_line) > 2:
                dev, field = split_line[1:3]
            else:
                dev, field = split_line[0:2]

            if dev not in devices:
                devices[dev] = QtWidgets.QTreeWidgetItem([dev])
                root.addChild(devices[dev])

            leaf = QtWidgets.QTreeWidgetItem([field])
            devices[dev].addChild(leaf)

        self.sortItems(0, 0)


class IOCWidget(Widget):
    def __init__(self, pv_list: str, parent=None):
        super().__init__(parent=parent)
        self.tree = IOCTreeWidget(pv_list)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.sp = QtWidgets.QLineEdit()
        self.subject = QtWidgets.QLineEdit()
        self.feedback = QtWidgets.QLabel()
        self.dump_path = QtWidgets.QLineEdit()
        self.dump = QtWidgets.QPushButton("Dump tree")

        self.setup()
        self.assign_events()
        self.set_theme()

    def assign_events(self) -> NoReturn:
        self.sp.returnPressed.connect(self.apply_val)
        self.dump.clicked.connect(self.dump_data)
        self.tree.itemSelectionChanged.connect(self.set_feedback)

    def dump_data(
        self, root: Node = None, indent: int = 0, file: IOBase = None
    ) -> NoReturn:  # todo: move to tree widget (or vice versa for plcwidgets.py?)
        # todo: add notification that this is a running-process (while it's running) - it takes forever
        path = self.dump_path.text()
        if not os.path.isdir(os.path.dirname(path)):
            Alert("Bad path", ("No such directory:", os.path.dirname(path)))
            return

        if file is None:
            if os.path.exists(path):
                Alert("Bad path", ("File already exists:", path))
                return
            with open(path, "w+") as file:
                logger.info(f"Writing to file {path}")
                self.setEnabled(False)
                n_roots = self.tree.topLevelItemCount()
                for i in range(n_roots):
                    self.dump_data(self.tree.topLevelItem(i), 0, file)
                self.setEnabled(True)
                logger.info(f"Finished writing to file {path}")
        else:
            pv = self.get_PV(root)
            val = self.get_val(pv)  # this will run caget on every PV
            val = f" = {str(val).strip()}" if val else ""

            file.write(f"{indent * ' '}{root.text(0)}{val}\n")

            for i in range(root.childCount()):
                self.dump_data(root.child(i), indent + 2, file)

    def get_PV(self, tree_item: Node = None) -> str:
        if tree_item is None:
            tree_item = self.tree.currentItem()

        if tree_item is None or tree_item.childCount() != 0:  # Item is not a leaf
            return ""

        family = []
        child = tree_item
        while child is not None:
            family.insert(0, child.text(0))
            child = child.parent()

        return ":".join(family) if family else ""

    def get_val(self, pv: str) -> Any:
        if not pv:
            return None
        self.subject.setText(pv)

        return_value = caget(pv)
        logger.debug(f"Try to get {pv}, received {return_value}")
        return return_value if return_value else None

    def set_feedback(self) -> NoReturn:
        pv = self.get_PV()

        if not pv:
            self.feedback.setText("")
        else:
            val = str(self.get_val(pv)).replace("\n", "")
            self.feedback.setText(val)

    def apply_val(self) -> NoReturn:
        pv = self.get_PV()
        if pv:
            try:
                caput(pv, self.sp.text())
                self.set_feedback()
            except Exception:  # fixme: identify exception(s)
                logger.exception()
                raise
