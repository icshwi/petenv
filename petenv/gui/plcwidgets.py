import logging
import os
from typing import NoReturn

from opcua.ua.uaerrors import BadAttributeIdInvalid
from PyQt5 import QtWidgets

from petenv.gui.widget import TreeWidget, Widget
from petenv.gui.window import Alert
from petenv.opc_client import OPCClient

logger = logging.getLogger()


class PLCTreeWidget(TreeWidget):
    def __init__(self, client, parent=None):
        super().__init__(name="PLC", parent=parent)
        self.client = client
        self.setup()

    def setup(self):
        super().setup()

        if self.client is None:
            root = QtWidgets.QTreeWidgetItem(["Could not connect to PLC"])
        else:
            parent = self.client.get_root_node()
            root = QtWidgets.QTreeWidgetItem([self.client.get_node_name(parent)])
            self.build_tree(root, parent)

        self.addTopLevelItem(root)

    def build_tree(self, branch, parent):
        children = parent.get_children()
        if not children:
            return

        for child in children:
            leaf = QtWidgets.QTreeWidgetItem([self.client.get_node_name(child)])
            branch.addChild(leaf)

        self.sortItems(0, 0)

    def dump_data(
        self, path, root=None, indent: int = 0, file=None, node=None
    ) -> NoReturn:
        if not os.path.isdir(os.path.dirname(path)):
            Alert("Bad path", ("No such directory:", os.path.dirname(path)))
            return

        if file is None:
            if os.path.exists(path):
                Alert("Bad path", ("File already exists:", path))
                return
            with open(path, "w+") as file:
                logger.info(f"Writing to file {path}")
                self.setEnabled(False)
                self.dump_data(path, root, 0, file)
                self.setEnabled(True)
                logger.info(f"Finished writing to file {path}")
        else:
            # First time dumpData is called, the node should be the root
            if root is None:
                root = self.topLevelItem(0)
                node = self.client.get_root_node()

            node_children = node.get_children()
            try:
                value = f" = {str(node.get_value())}" if not node_children else ""
            except BadAttributeIdInvalid:
                value = ""

            if len(value) > 60:
                value = value[0:50] + "..." + value[-10:]

            file.write(f"{indent * ' '}{root.text(0)}{value}\n")

            for i in range(root.childCount()):
                self.dump_data(path, root.child(i), indent + 2, file, node_children[i])


class PLCWidget(Widget):
    def __init__(self, ip, parent=None):
        super().__init__()
        self.client = OPCClient(ip)

        try:
            self.client.connect()
            self.tree = PLCTreeWidget(self.client)
            self.selected_node = self.client.get_root_node()
        except Exception:  # todo: catch specific exceptions
            logger.exception()
            self.client = None
            self.tree = PLCTreeWidget(self.client)
            self.selected_node = None

        self.selected = self.tree.currentItem()
        self.sp = QtWidgets.QLineEdit()
        self.subject = QtWidgets.QLineEdit()
        self.feedback = QtWidgets.QLabel()
        self.dump_path = QtWidgets.QLineEdit()
        self.dump = QtWidgets.QPushButton("Dump Tree")
        self.layout = QtWidgets.QVBoxLayout(self)

        self.setup()
        if self.client is not None:
            self.assign_events()
        self.set_theme()

    def assign_events(self):
        self.sp.returnPressed.connect(
            lambda: self.client.apply_node_value(
                self.selected_node, self.sp.text(), self.feedback
            )
        )

        self.dump.clicked.connect(lambda: self.tree.dump_data(self.dump_path.text()))
        self.tree.itemSelectionChanged.connect(self.evolve_tree)

    def evolve_tree(self):
        current = self.tree.currentItem()

        parents = []
        while current.text(0) != "0:Root":
            parents.insert(0, current.text(0))
            current = current.parent()

        command_str = "client.get_root_node()"
        node = self.client.get_root_node()

        if len(parents) > 0:
            command_str = f"{command_str}.get_child({str(parents)})"

        for parent in parents:
            node = node.get_child(parent)

        self.selected_node = node
        try:
            self.feedback.setText(str(node.get_value()))
        except BadAttributeIdInvalid:
            pass

        self.subject.setText(command_str)
        self.subject.resize(self.tree.width(), self.subject.height())

        if not parents:
            return
        if parents in self.client.expand_list:
            return

        self.client.expand_list.append(parents)

        children = self.client.get_root_node().get_child(parents).get_children()
        branch = self.tree.currentItem()
        for child in children:
            leaf = QtWidgets.QTreeWidgetItem([self.client.get_node_name(child)])
            branch.addChild(leaf)

    def disconnect(self) -> NoReturn:
        if self.client is not None:
            self.client.disconnect()
