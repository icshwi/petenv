import argparse
import logging
import sys

from PyQt5 import QtWidgets

from petenv.gui.iocwidgets import IOCWidget
from petenv.gui.plcwidgets import PLCWidget
from petenv.gui.window import MainWindow


def main():
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename="run.log",
        filemode="w",
        level=logging.DEBUG,
    )

    parser = argparse.ArgumentParser(description="OPC UA (and CA) navigator")
    parser.add_argument("ip", type=str, help="PLC IP address")
    parser.add_argument("-p", "--pvs", type=str, help="Path to PV list file")
    args = parser.parse_args()

    app = QtWidgets.QApplication([])
    window = MainWindow()

    plc = PLCWidget(ip=args.ip)
    window.add_widget(plc)

    if args.pvs:
        ioc = IOCWidget(args.pvs)
        window.add_widget(ioc)

    app.aboutToQuit.connect(plc.disconnect)
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    if sys.version_info < (3, 6, 0):
        sys.stderr.write("You need python 3.6 or later to run this script\n")
        sys.exit(1)

    main()
