import os
from abc import abstractmethod
from typing import NoReturn

from PyQt5 import QtGui, QtWidgets


class TreeWidget(QtWidgets.QTreeWidget):
    def __init__(self, name: str = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if name is not None:
            self.name = name

    def setup(self):
        self.setExpandsOnDoubleClick(True)
        self.setAnimated(True)
        self.setColumnCount(1)
        self.setHeaderLabel(self.name)

    @abstractmethod
    def build_tree(self, branch, parent) -> NoReturn:
        pass


class Widget(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def setup(self) -> NoReturn:
        self.layout.setContentsMargins(0, 0, 0, 0)

        # Set point and feedback boxes
        value_field = QtWidgets.QWidget()
        value_field_hbox = QtWidgets.QHBoxLayout(value_field)
        self.sp.setFixedWidth(120)
        self.feedback.setSizePolicy(
            QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred
        )

        value_field_hbox.addWidget(self.sp)
        value_field_hbox.addWidget(self.feedback)
        value_field_hbox.setContentsMargins(0, 0, 0, 0)

        # Dump tree path and button
        path_field = QtWidgets.QWidget()
        path_field_hbox = QtWidgets.QHBoxLayout(path_field)

        self.dump_path.setText(os.path.dirname(os.getcwd()) + "/dumpfile.txt")

        path_field_hbox.addWidget(self.dump_path)
        self.dump.setFixedWidth(100)
        path_field_hbox.addWidget(self.dump)
        path_field_hbox.setContentsMargins(0, 0, 0, 0)

        self.layout.addWidget(self.subject)
        self.layout.addWidget(value_field)
        self.layout.addWidget(path_field)
        self.layout.addWidget(self.tree)

    @abstractmethod
    def assign_events(self):
        pass

    def set_theme(self) -> NoReturn:
        default_font = QtGui.QFont("Monospace", 10)

        self.tree.header().setFont(QtGui.QFont("Monospace", 10, QtGui.QFont.Bold))
        self.subject.setPlaceholderText("Command")
        self.sp.setPlaceholderText("set value")

        self.tree.setFont(default_font)
        self.subject.setFont(default_font)
        self.sp.setFont(default_font)
        self.feedback.setFont(default_font)
        self.dump.setFont(default_font)
        self.dump_path.setFont(default_font)
