import logging
from typing import Any, NoReturn

from opcua import Client, Node, ua

logger = logging.getLogger()


class OPCClient(Client):
    def __init__(self, ip: str, timeout: float = 5.0) -> NoReturn:
        """Initialize OPCUA client.

        :param ip: PLC IP address.
        :param timeout: Timeout in seconds for connection.
        """
        url = "opc.tcp://{}:4840".format(ip)
        super().__init__(url, timeout=timeout)
        self.expand_list = []
        self.selected = self.get_root_node()

    @staticmethod
    def set_node_value(node: Node = None, value: Any = None) -> NoReturn:
        """Set value to OPCUA node."""
        if node is None or value is None:
            raise ValueError
        variant_type = node.get_data_type_as_variant_type()
        variant = ua.uatypes.Variant(value, variant_type)
        data_value = ua.DataValue()
        data_value.Value = variant
        node.set_data_value(data_value, variant_type)

    @staticmethod
    def get_node_value(node: Node = None):
        """Return node value.

        This function merely translates opcua package's 'get_value' to a
        syntax following this class. This is needed as the 'setValue'
        method differs from the opcua package's node function
        'set_value'.
        """
        if node is None:
            raise ValueError
        return node.get_value()

    @staticmethod
    def get_node_name(node: Node = None) -> str:
        """Returns name of node"""
        if node is None:
            raise ValueError
        return (
            str(node.get_browse_name()).split("(")[1].split(")")[0]
        )  # todo: make more clear

    def apply_node_value(
        self, node: Node, val: Any, feedback
    ) -> NoReturn:  # todo: add type hints
        """Value setter including error handling.

        This function is mainly used by the petenv gui.
        """
        try:
            self.set_node_value(node, type(node.get_value())(val))
            feedback.setText(str(node.get_value()))
        except Exception as e:
            logger.exception()
            print(f"Could not set node value: {e}")
