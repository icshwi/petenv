from abc import abstractmethod
from typing import NoReturn

from petenv.opc_client import OPCClient


class OPCDevice:
    """Wrapper class for OPCClient

    :param client: OPCUA client connected to PLC.
    """

    def __init__(self, client: OPCClient):
        self.client = client

    @abstractmethod
    def run(self) -> NoReturn:
        pass

    @property
    @abstractmethod
    def name(self) -> str:
        pass
