from typing import NoReturn

from opcua import Node

from petenv.opc_client import OPCClient
from petenv.sim.opcdevice import OPCDevice


class CV(OPCDevice):
    def __init__(self, client: OPCClient, cmd_openness: Node, fb_openness: Node):
        """Control Valve

        :param client: OPCUA client connected to PLC.
        :param cmd_openness: OPCUA node of valve's openness command in %.
        :param fb_openness: OPCUA node of valve's openness feedback in %.
        """
        super().__init__(client=client)
        self.cmd_openness = cmd_openness
        self.fb_openness = fb_openness

    def run(self) -> NoReturn:
        """ Run infinite loop, updating values of device."""
        while True:
            self.client.set_node_value(self.fb_openness, self.cmd_openness.get_value())

    @property
    def name(self) -> str:
        """Returns name of device"""
        return self.fb_openness.get_display_name().Text.split("_")[1]
