import random
import time
from typing import NoReturn, Tuple

from opcua import Node

from petenv.opc_client import OPCClient
from petenv.sim.opcdevice import OPCDevice


class Analog(OPCDevice):
    def __init__(self, client: OPCClient, node: Node, *, limits: Tuple[float, float]):
        """Analog device

        :param client: OPCUA client connected to PLC.
        :param node: OPCUA node of analog device.
        :param limits: Value pair between which values will be randomized.
        """
        super().__init__(client=client)
        self.limits = limits
        self.node = node

    def run(self) -> NoReturn:
        """ Run infinite loop, updating with random values of device."""
        while True:
            value = random.randint(*self.limits)
            self.client.set_node_value(self.node, value)
            time.sleep(0.2)

    @property
    def name(self) -> str:
        """Returns name of Node"""
        return self.node.get_display_name().Text.split("_")[1]
