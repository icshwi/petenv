import time
from typing import NoReturn

from opcua import Node

from petenv.opc_client import OPCClient
from petenv.sim.opcdevice import OPCDevice


class YSV(OPCDevice):
    def __init__(
        self,
        client: OPCClient,
        energized_node: Node,
        opened_node: Node,
        closed_node: Node,
    ):
        """On-Off Valve

        YSV probably refers to Safety-related Y-Valve.  # fixme: someone should revise this

        :param client: OPCUA client connected to PLC.
        :param energized_node: OPCUA node of valve's 'energize' signal.
        :param opened_node: OPCUA node of valve's 'opened' signal.
        :param closed_node: OPCUA node of valve's 'closed' signal.
        """
        super().__init__(client=client)
        self.energized_node = energized_node
        self.opened_node = opened_node
        self.closed_node = closed_node

    def run(self) -> NoReturn:
        """Run infinite loop, updating values of device.

        This simulator handles both valves that energize to open, and
        valves that energize to close.

        Caveat: PLC tag for energize signal must end with 'open' or
        'close', indicating the function of energizion.
        """
        # todo: refactor all of the following
        oc = [
            self.opened_node,
            self.closed_node,
        ]
        name = self.energized_node.get_display_name().Text  # 'energized' node name
        move_time = 0.7  # Time in seconds to open/close valve

        # Determine if energize to open or energize to close
        energise_type = 0 if "open" in name else 1

        # Infinite loop to check if energization is not matching the state of
        # the valve, remove current state, wait for simulated move time and set
        # new state.
        while True:
            if self.energized_node.get_value() and not oc[energise_type].get_value():
                self.client.set_node_value(oc[1 - energise_type], False)
                time.sleep(move_time)
                self.client.set_node_value(oc[energise_type], True)

            if (
                not self.energized_node.get_value()
                and not oc[1 - energise_type].get_value()
            ):
                self.client.set_node_value(oc[energise_type], False)
                time.sleep(move_time)
                self.client.set_node_value(oc[1 - energise_type], True)

            time.sleep(0.2)

    @property
    def name(self) -> str:
        """Returns name of device"""
        return self.energized_node.get_display_name().Text.split("_")[1]
