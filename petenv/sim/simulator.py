"""Simulator script

Modifies all identified PLC-controlled devices more or less continuously.
"""

import argparse
import threading
from typing import NoReturn, Sequence

from opcua import Node

from petenv.opc_client import OPCClient
from petenv.sim.analog import Analog
from petenv.sim.cv import CV
from petenv.sim.opcdevice import OPCDevice
from petenv.sim.ysv import YSV


class SimThread(threading.Thread):
    """Simulation thread"""

    def __init__(self, thread_id: int, name: str, sim: OPCDevice):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.sim = sim

    def run(self) -> NoReturn:
        print("Starting " + self.name)
        self.sim.run()
        print("Exiting " + self.name)


def main():
    parser = argparse.ArgumentParser(description="Simulator")
    parser.add_argument("ip", type=str, help="PLC IP address")
    args = parser.parse_args()

    # Create and connect client
    client = OPCClient(ip=args.ip)
    client.connect()

    # Get all TT, PT and RT nodes
    objects: Sequence[Node] = (
        client.get_root_node().get_child("0:Objects").get_children()
    )
    plc: Node = objects[-1]
    inputs: Node = plc.get_child("3:Inputs")
    outputs: Node = plc.get_child("3:Outputs")

    analog_tags = ("_TT-", "_PT-", "_RT-", "_FT")  # Analog tags to look for
    analog_devices = []
    for node in inputs.get_children():
        node_name = node.get_display_name().Text  # Node name
        for tag in analog_tags:
            if tag in node_name and node_name not in analog_devices:
                # Create simulator objects
                analog_devices.append(Analog(client, node, limits=(13000, 14000)))

    valves = []
    valve_tags = []
    for node in outputs.get_children():
        node_name = node.get_display_name().Text

        try:
            pid_tag = node_name.split("_")[1]  # P&ID tag
        except IndexError:
            pid_tag = node_name

        if pid_tag in valve_tags:
            continue

        if "YSV" in node_name and node_name not in valves:
            e_node = outputs.get_child(node_name)  # 'energize'
            # todo: list all children and then match
            o_node = inputs.get_child("hwi_" + pid_tag + "_opened")  # 'opened'
            c_node = inputs.get_child("hwi_" + pid_tag + "_closed")  # 'closed'
            valves.append(YSV(client, e_node, o_node, c_node))
            valve_tags.append(pid_tag)
        elif "CV-" in node_name and node_name not in valves:
            # todo: list all children and then match
            o_node = outputs.get_child("hwo_" + pid_tag)  # 'open'
            i_node = inputs.get_child("hwi_" + pid_tag)  # 'openness'
            valves.append(CV(client, o_node, i_node))
            valve_tags.append(pid_tag)

    # Create and start threads
    for device in analog_devices:
        SimThread(thread_id=1, name=device.name, sim=device).start()

    for valve in valves:
        SimThread(thread_id=1, name=valve.name, sim=valve).start()


if __name__ == "__main__":
    import sys

    if sys.version_info < (3, 6, 0):
        sys.stderr.write("You need python 3.6 or later to run this script\n")
        sys.exit(1)

    main()
