from setuptools import find_packages, setup

setup(
    name="petenv",
    version="1.0.4",
    use_scm_version=True,
    setup_requires=["setuptools_scm==6.4.2"],
    description="A python test environment for plc/epics",
    url="https://gitlab.esss.lu.se/icshwi/petenv",
    author="Johannes Kazantzidis",
    author_email="johannes.kazantzidis@ess.eu",
    license="GPL",
    entry_points={
        "console_scripts": [
            "petenv-nav=petenv.gui.navigator:main",
            "petenv-sim=petenv.sim.simulator:main",
        ]
    },
    packages=find_packages(),
    python_requires=">=3.6",
    install_requires=[
        "opcua",
        "pyepics",
        "cryptography",
        "pyqt5==5.15.2",
        "PyQt5-sip",
        "pytest-metadata",
        "pytest==6.2.1",
        "pytest-parallel",
        "cffi",
        "pytest-html",
        "pdoc3",
	"gitpython==3.1.12",
    ],
    zip_safe=False,
)
