import os

from _pytest.config import Config
from epics import caget
from git import InvalidGitRepositoryError, NoSuchPathError, Repo
from git.cmd import Git

from petenv.opc_client import OPCClient


def pytest_configure(config: Config):
    """Configuration for reports.

    When running a test with the '--metadata petenv' flag, this code
    will be run to add metadata into the top of the generated
    report. Note that if the variables below are `None`, petenv will ask
    you for the information in the command line interface. You may
    hardcode the values to avoid this command line interaction if you prefer.
    """
    # Hard code these values to avoid being asked when running petenv with
    # metadata flag. ###########################################################
    plc_ip: str = None  # PLC ip address e.g. "172.30.4.163"
    petenv_repo_path: str = None  # Path to petenv e.g. "~/petenv"
    plcfactory_repo_path: str = None  # Path to plcfactory e.g. "~/ics_plc_factory/"
    opi_repo_path: str = None  # Path to the ess-opis repo e.g. "~/ess-opis/"
    path_to_pv_list: str = None  # Path to a list of PVs e.g. "~/thccs/THCCS_PVs.list"
    versiondog_version: str = None  # PLC project version in versiondog e.g. "6"
    versiondog_path_to_project: str = None  # Path to plc project in versiondog e.g.
    # "\=ESS\INFR [Infrastructure]\ICS_MASTER_LIBRARY\TIA Portal"
    ############################################################################

    if "petenv" in config._metadata:
        # petenv
        verify_repo(config, "Path to petenv repo: ", " petenv", petenv_repo_path)

        # plc factory
        verify_repo(
            config, "Path to PLC Factory repo: ", " plc factory", plcfactory_repo_path
        )

        # opi
        verify_repo(config, "Path to OPI_REPO repository: ", " OPI_REPO", opi_repo_path)

        # plc ip
        if plc_ip is None:
            plc_ip = input("PLC IP: ")

        if plc_ip:
            config._metadata[" PLC IP"] = plc_ip
            client = OPCClient(plc_ip)
            client.connect()
            objects = client.get_root_node().get_child("0:Objects")
            children = objects.get_children()
            cpu = children[-1]

            metadata_dict = {
                " PLC softwareRevision": "2:SoftwareRevision",
                " PLC SerialNumber": "2:SerialNumber",
                " PLC OrderNumber": "3:OrderNumber",
                " PLC HardwareRevision": "2:HardwareRevision",
            }
            for key, val in metadata_dict:
                config._metadata[key] = cpu.get_child(val).get_value()

            config._metadata[" PLC Model"] = str(
                cpu.get_child("2:Model").get_value()
            ).split()[-2]
            client.disconnect()
        else:
            config._metadata[" PLC attributes"] = "Ignored"

        # versiondog
        if versiondog_path_to_project is None:
            versiondog_path_to_project = input("PLC program path in VersionDog: ")
        config._metadata[" PLC program path in VersionDog"] = (
            versiondog_path_to_project if versiondog_path_to_project else "Ignored"
        )

        if versiondog_version is None:
            versiondog_version = input("PLC program version in VersionDog: ")
        config._metadata[" PLC program version in VersionDog"] = (
            versiondog_version if versiondog_version else "Ignored"
        )

        # pv list
        if path_to_pv_list is None:
            path_to_pv_list = input("Path to pvlist: ")

        path_to_pv_list = os.path.expanduser(path_to_pv_list)
        if path_to_pv_list:
            with open(path_to_pv_list, "r") as dbl_file:
                pv_list = dbl_file.readlines()

            for pv_name in pv_list:
                if "REQMOD" in pv_name:
                    sys, disdevidx, _ = pv_name.split(":")
                    prefix = "{}:{}".format(sys, disdevidx)

                    version_dict = {
                        " IOC asyn version": ":asyn_VER",
                        " IOC autosave version": "autosave_VER",
                        " IOC s7 version": ":s7plc_VER",
                        " IOC modbus version": ":modbus_VER",
                        " IOC require version": ":require_VER",
                    }
                    for key, val in version_dict.items():
                        config._metadata[key] = caget(prefix + val)
                    break
        else:
            config._metadata[" EPICS/IOC attributes"] = "Ignored"


def verify_repo(
    config: Config, instruction: str, description: str, repo_path: str
):  # todo: fix this function name; 'verify' and no return
    repo_ok = False
    while not repo_ok:
        if repo_path is None:
            repo_path = input(instruction)
        if not repo_path:
            config._metadata[instruction] = "Ignored"
            break
        else:
            try:
                set_commit(config, description, repo_path)
                repo_ok = True
            except InvalidGitRepositoryError:
                answer = input("Invalid repository path, retry? [y/n]: ")
                if answer.lower() in ["y", "yes", "yeah"]:
                    repo_path = input(instruction)
                    continue
                elif answer.lower() in ["n", "no", "nope"]:
                    break


def set_commit(config, tag, path):
    try:
        repo = Repo(path)
        url = str(Git(path).remote(verbose=True)).split()[1]
        ref = str(repo.head.reference.log()[-1]).split()[1]
        config._metadata["{} url".format(tag)] = url
        config._metadata["{} git commit".format(tag)] = ref
    except NoSuchPathError:
        print(f"{path} is not a git repository")
