import time


def lower_case(text: str) -> str:
    """Returns lower case version of input string.

    This is a longer test description. You can make these type of
    descriptions to classes as well as functions.

    :param text: A text string to convert to all lower case
    :return: The lower cased version of the input argument
    """
    time.sleep(3)
    return text.lower()


def add(a: float, b: float) -> float:
    """Sums two numbers.

    :param a: A number.
    :param b: A number.
    :return: Sum of the two numbers
    """
    time.sleep(3)
    return a + b


def test_lower():
    """All methods with names starting with 'test_' will be called when
    running the test script
    """
    assert lower_case("HeLlO WoRlD") == "hello world"


def test_add():
    """Verifies that 2 + 5 = 7"""
    assert add(2, 5) == 7
