import logging
import time
from typing import Tuple

import pytest
from epics import ca, caget, caput
from opcua import Node

from petenv.opc_client import OPCClient

QUIET = True
IP = input("\nPLC IP: ")

logger = logging.getLogger()


def quiet_mode(msg_bytes):
    """Quench pyepics warnings

    When running over VPN, two networks are enabled. The client library will
    find the PV on the same IOC twice, from different IP addresses. This ambiguity
    results in the following warning:

    CA.Client.Exception...............................................
        Warning: "Identical process variable names on multiple servers"
        Context: "Channel: "Tgt-HeIn1013:Proc-YSV-033:ClosingTime",
        Connecting to: L480:35519, Ignored: L480:35519"
        Source File: ../cac.cpp line 1320
        Current Time: Fri Mar 20 2020 10:51:04.209830858
    ..................................................................

    As this is no problem for this test, this function serves the
    purpose to quench such warnings in order to not clutter the pytest
    terminal output.
    """
    pass  # Alternatively, we could write the warnings into a log file.


if QUIET:
    ca.replace_printf_handler(quiet_mode)  # Replace pyepics handler


@pytest.fixture(scope="module")
def com():
    """Setup OPCUA client.

    Create OPCUA client and connect to PLC upon starting test. When the
    test is finished, disconnect the client.
    """
    client = OPCClient(IP)
    client.connect()

    # Get the plc node
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]  # Node for PLC

    # Provide the fixture value. Everything after this is teardown code
    yield client, plc
    client.disconnect()


def wait(pv: str, value: float, timeout: float = 4.0):
    """Wait for PV value.

    Run `caget` on a PV until expected value is seen, or until timeout.

    :param pv: EPICS PV name
    :param value: Expected PV value
    :param timeout: Timeout in seconds
    """
    tries = 0
    delay = 0.2
    while caget(pv) != value and tries < timeout / delay:
        tries += 1
        time.sleep(delay)


def get_analogs():
    """Return list of all analog transmitter PV names.

    Note that this function searches for devices with 'T-' in it. If
    found, petenv assumes it's a transmitter. If this is not always the
    case, this function must be modified.
    """
    # Connect to opcua server (this function cannot use the fixture)
    client = OPCClient(IP)
    client.connect()

    # Get the plc node
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]  # Node for PLC

    # Find all analog transmitters
    transmitters = []
    instances = plc.get_child("3:DataBlocksInstance").get_children()
    for node in instances:
        node_name = node.get_display_name().Text
        if (
            "_iDB" in node_name
            and node_name not in transmitters
            and ("T-" in node_name or "E-" in node_name)
        ):
            transmitters.append(node_name.split("_")[1])  # P&ID tag

    client.disconnect()

    logger.warning(f"Valves: {transmitters}")
    return transmitters


def get_valves():
    """Return list of all YSV valves PV names.

    Note that this function is programmed to only search for valves
    called 'YSV'. Consider adding more names if needed.
    """
    # Connect to opcua server (this function cannot use the fixture)
    client = OPCClient(IP)
    client.connect()

    # Get the plc node
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]  # Node for PLC

    # Find all YSV valves
    valves = []
    instances = plc.get_child("3:DataBlocksInstance").get_children()
    for node in instances:
        node_name = node.get_display_name().Text
        if "YSV" in node_name and "_iDB" in node_name and node_name not in valves:
            valves.append(node_name.split("_")[1])  # P&ID tag

    client.disconnect()

    logger.warning(f"Valves: {valves}")
    return valves


# @pytest.mark.skip(reason="just wanna test valves now")
@pytest.mark.parametrize("pv", get_analogs())
def test_transmitter_alarms(com: Tuple[OPCClient, Node], pv: str):
    """Verify analog transmitter alarms.

    Verify HIHI, HI, LO, LOLO, Overrange and Underrange signals

    :param com: OPCUA client, plc node
    :param pv: PV name of transmitter to be tested
    """
    if not pv:
        return

    client, plc = com
    caput("{}:Cmd_FreeRun".format(pv), 1)
    pid_tag = "{}-{}".format(pv.split("-")[-2], pv.split("-")[-1]).lower()

    # Find input signal name
    analogue_input = None
    for child in plc.get_child("3:Inputs").get_children():
        if pid_tag in child.get_display_name().Text.lower():
            analogue_input = child
            break
    if not analogue_input:
        return

    scale_low = caget("{}:ScaleLOW".format(pv))
    scale_high = caget("{}:ScaleHIGH".format(pv))
    ciel = 27648
    offset = 10
    hihi_lim = caget("{}:FB_Limit_HIHI".format(pv))
    hi_lim = caget("{}:FB_Limit_HI".format(pv))
    lo_lim = caget("{}:FB_Limit_LO".format(pv))
    lolo_lim = caget("{}:FB_Limit_LOLO".format(pv))
    delay = 1
    logger.warning(f"HIHI limit: {hihi_lim}")
    logger.warning(f"HI limit: {hi_lim}")
    logger.warning(f"LOLO limit: {lo_lim}")
    logger.warning(f"LO limit: {lolo_lim}")

    # Test overrange and io error
    client.set_node_value(analogue_input, 30000)
    time.sleep(delay)
    assert caget("{}:Overrange".format(pv)) == 1
    assert caget("{}:IO_Error".format(pv)) == 1

    # Test underrange and io error
    client.set_node_value(analogue_input, -1)
    time.sleep(delay)
    assert caget("{}:Underrange".format(pv)) == 1
    assert caget("{}:IO_Error".format(pv)) == 1

    # Test HIHI alarm
    if hihi_lim != scale_high:
        hihi = ciel * (hihi_lim - scale_low) / (scale_high - scale_low) + offset
        client.set_node_value(analogue_input, int(hihi))
        wait("{}:HIHI".format(pv), 1)
        assert caget("{}:HIHI".format(pv)) == 1

    # Test HI alarm
    if hi_lim != scale_high:
        hi = ciel * (hi_lim - scale_low) / (scale_high - scale_low) + offset
        client.set_node_value(analogue_input, int(hi))
        wait("{}:HI".format(pv), 1)
        assert caget("{}:HI".format(pv)) == 1

    # Test LO alarm
    if lo_lim != scale_low:
        lo = ciel * (lo_lim - scale_low) / (scale_high - scale_low) - offset
        client.set_node_value(analogue_input, int(lo))
        wait("{}:LO".format(pv), 1)
        assert caget("{}:LO".format(pv)) == 1

    # Test LOLO alarm
    if lolo_lim != scale_low:
        lolo = ciel * (lolo_lim - scale_low) / (scale_high - scale_low) - offset
        client.set_node_value(analogue_input, int(lolo))
        wait("{}:LOLO".format(pv), 1)
        assert caget("{}:LOLO".format(pv)) == 1

    # Check if values seem to be unset
    if hihi_lim == 0 and hi_lim == 0 and lo_lim == 0 and lolo_lim == 0:
        print("The limits for this device do not seem to be configured.")
        print("All limits are currently set to 0.")

    # Set non-alarming value
    nominal_lim = lo_lim + (hi_lim - lo_lim) / 2
    nominal = ciel * (nominal_lim - scale_low) / (scale_high - scale_low)
    client.set_node_value(
        analogue_input, int(nominal)
    )  # Set a value that is not an alarm


# @pytest.mark.skip(reason="just wanna test transmitters now")
@pytest.mark.parametrize("pv", get_valves())
def test_pv_valve_alarms(com: Tuple[OPCClient, Node], pv: str):
    """Verify solenoid valve alarms.

    Verify that opening timeout, closing timeout and IO error is working
    properly. Note that IO error is true if the valves 'opened' and
    'closed' signals are simultaneously true.

    :param com: OPCUA client, plc node
    :param pv: PV name of valve to be tested
    """
    client, plc = com

    pid_tag = "{}-{}".format(pv.split("-")[-2], pv.split("-")[-1]).lower()
    caput("{}:Cmd_Force".format(pv), 1)
    input_children = plc.get_child("3:Inputs").get_children()
    opened = [
        child
        for child in input_children
        if f"{pid_tag}_opened" in child.get_display_name().Text.lower()
    ]
    closed = [
        child
        for child in input_children
        if f"{pid_tag}_closed" in child.get_display_name().Text.lower()
    ]
    logger.warning(f"Opened: {opened}")
    logger.warning(f"Closed: {closed}")
    if not opened or not closed:
        return
    opened, closed = opened[0], closed[0]

    close_pv = "{}:Cmd_ForceClose".format(pv)
    open_pv = "{}:Cmd_ForceOpen".format(pv)
    opening_time = caget("{}:ClosingTime".format(pv)) / 1000 + 2  # ms to s + offset
    closing_time = caget("{}:ClosingTime".format(pv)) / 1000 + 2  # ms to s + offset
    opening_timeout_pv = "{}:Opening_TimeOut".format(pv)
    closing_timeout_pv = "{}:Closing_TimeOut".format(pv)

    # Close valve and remove any prevailing alarm
    caput(close_pv, 1)
    client.set_node_value(closed, True)
    client.set_node_value(opened, False)
    wait("{}:Closed".format(pv), 1)
    wait("{}:Opened".format(pv), 0)
    caput("{}:Cmd_AckAlarm".format(pv), 1)

    wait("{}:GroupAlarm".format(pv), 0)
    assert caget("{}:GroupAlarm".format(pv)) == 0

    # Run both open and close actions, and check timeouts on each
    caput(open_pv, 1)  # Command open
    time.sleep(opening_time)  # Wait until opening timeout

    assert caget(opening_timeout_pv) == 1  # Verify timeout alarm

    client.set_node_value(closed, False)  # Remove closed signal
    client.set_node_value(opened, True)  # Set opened signal
    wait("{}:Opened".format(pv), 1)  # Wait for new state to take effect
    wait("{}:Closed".format(pv), 0)  # Wait for new state to take effect
    caput("{}:Cmd_AckAlarm".format(pv), 1)  # Acknowledge alarm

    caput(close_pv, 1)  # Command close
    time.sleep(closing_time)  # Wait until closing timeout

    assert caget(closing_timeout_pv) == 1  # Verify timeout alarm

    client.set_node_value(closed, True)  # Set closed signal
    client.set_node_value(opened, False)  # Remove opened signal
    wait("{}:Opened".format(pv), 0)  # Wait for new state to take effect
    wait("{}:Closed".format(pv), 1)  # Wait for new state to take effect
    caput("{}:Cmd_AckAlarm".format(pv), 1)  # Acknowledge alarm

    # Verify alarm if both 'opened' and 'closed' signals are prevailing
    client.set_node_value(closed, True)
    client.set_node_value(opened, True)
    wait("{}:Opened".format(pv), 1)
    wait("{}:Closed".format(pv), 1)

    assert caget("{}:IO_Error".format(pv)) == 1

    # Close valve and remove any prevailing alarm
    caput(close_pv, 1)
    client.set_node_value(closed, True)
    client.set_node_value(opened, False)
    wait("{}:Opened".format(pv), 0)  # Wait for new state to take effect
    wait("{}:Closed".format(pv), 1)  # Wait for new state to take effect
    caput("{}:Cmd_AckAlarm".format(pv), 1)
    wait("{}:GroupAlarm".format(pv), 0)

    assert caget("{}:GroupAlarm".format(pv)) == 0
