# example_scenario

This folder contains a `conftest.py`-file that can be symlinked or copied to other folders, and used as a "framework" setup for test scenarios.

It also contains test procedures specifically written for the THCC system, as well as some example tests that essentially are there for demonstrative (and currently CI/CD) purposes.
