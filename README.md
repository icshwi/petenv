# Disclaimer: Petenv is a deprecated tool and should not be used for PLC Unit Test. Please contact Automation Section for information on Unit Test development. 


# `P`LC-`E`PICS `t`est `env`ironment (*petenv*)

*petenv* includes necessary packages to make automated tests of the the communication chain from PLC to EPICS. This implies full system testing including control system behavior on the PLC level as well as OPerator Interfaces (OPIs), alarms and other EPICS services. Furthermore, *petenv* comes with a couple of aiding utilities:
- An OPC UA device and EPICS PV navigator---a graphical interface, allowing the user to browse tree structures of both PLC and IOC in parallel, and manipulate values
- A simple OPC UA device simulator of, e.g., transmitters and on-off valves
- A generic alarm test script, finding all transmitters and on-off valves in a system and verifying that all alarms work and no conflicts are found (e.g. a HIHI alarm being lower than a HI alarm)

*petenv* utilizes the Python3 library [`pytest`](https://docs.pytest.org/en/latest/), which makes it easy to write small tests, yet scales to support complex functional testing for applications and libraries. To aid communication, [`opcua`](https://python-opcua.readthedocs.io/en/latest/) is used for direct communication with PLCs (or any OPCUA server) and [`pyepics`](https://github.com/pyepics/pyepics) for direct communication with EPICS IOCs.

![Test](docs/testing.gif)

## Prerequisites

- Python 3.6 or higher

## Quickstart

### Install

```shell script
$ pip install --upgrade pip  # important for PyQt5
$ pip install .
```

### Petenv navigator

```shell script
$ pytest-nav <ip> [-p <pv_list_file>]
```

If you do not provide the path to a PV list file, the application will open as a OPC UA browser only.

![Gui](docs/gui.gif)

### Petenv simulator

```shell script
$ pytest-sim <ip>
```

### Test suite

```shell script
$ pytest -vs example_scenario/test_script_example.py
```

## Configuration

Configuration is optional but recommended. For testing with `pytest` it is recommended to make a copy (or symlink) of the `conftest.py` file into the local directory of your test scrips. `conftest.py` will ask you for system information during runtime if needed. However, you can avoid this by editing the file (the file contains instructions inside, on how to do so).

Similarly, `alarm_tests.py` will ask for the ip address of you PLC upon running, and can also be edited in the file to avoid being asked for ip every time.

Caveat: These standard tests are work in progress. The purpose is to provide standardized simple tests to e.g. test all transmitter alarms. The script will, based on the provided IP address in `conftest.py` find all transmitters in your PLC program and utilize both OPCUA and Channel Access to verify that all alarms work as expected, e.g. HIHI, HI, LO, LOLO, IO-Error for transmitters and opening timeout, closing timeout and IO-Error for solenoid valves.

### Generating a test report

*petenv* also utilizes pytest-html to auto-generate test reports. This can be run as follows:

```shell script
pytest -vs scenarios/test_script_example.py --html=report.html
```

### Read more

For more information on *petenv*, see [docs/README.md](docs/README.md).
