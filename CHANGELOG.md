# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2020-12-22
### Added
- Executable for navigator: `petenv-nav`
- Executable for simulator: `petenv-sim`
- GPL license (due to use of `PyQt5`)
- Changelog
- Build status badge on repository
- Pre-commit hook to the git repository
- Type hints
- Warning if you try to dump to existing file
- Hooks for: import sort, EOF-fixer, and trailing whitespace to pre-commit
- A `requirements-dev.txt` for development
- SonarQube integration for code analysis

### Changed
- Now builds (and distributes) the PyPi package using a Python 3.6 image instead of 3.7
- Moved example test cases `petenv/test/` to `scenarios/`
- The navigator when used without a PV list (`-p <pv_list>`) now opens an OPC UA-only browse window
- Started rewriting documentation
- Docstrings now follow reST
- Build pipeline so it runs some real functions and not just linting and formatting
- Started making code more pythonic
- Started refactoring code
- Moved `.gif` files to `docs/`
- `.flake8` config for more *black* exceptions

### Removed
- Allowing pushing to master
- Pre-commit as part of gitlab CI pipeline
- Colouring of GUI elements


[Unreleased]: https://gitlab.esss.lu.se/icshwi/petenv/compare/1.1.1...HEAD
[1.1.1]: https://gitlab.esss.lu.se/icshwi/petenv/compare/1.0.3...1.1.1
